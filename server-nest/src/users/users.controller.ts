import { Controller, Req, Res, Get, Post, Put, Body, Param } from "@nestjs/common";
import { UsersService } from "./users.service";
import { IUser } from "./users.model";

@Controller('api/users')
export class UsersController {

    constructor(private readonly usersSevice: UsersService) {}

    @Get()
    async getAllUsers() {
        const users: IUser[] = await this.usersSevice.getAllUsers();
        return users;
    }

    @Get('logout')
    logOutUser(@Res() res:any, @Req() req: any): void {
        try {
            console.log('Было', req.cookies);
            res.clearCookie('token');
            return res.send();
        } catch(e) {
            console.log(e.message);
        }
    }

    @Get(':id/bag')
    async getBag(@Param('id') id: string) {
        const bag = this.usersSevice.getBag(id);
        return bag;
    }

    @Post('login')
    async loginUser(
        @Req() req: any,
        @Res() res: any,
        @Body('email') email: string,
        @Body('password') password: string
        ) {
            const { user, token }: {user: IUser, token: string} = await this.usersSevice.loginUser(email, password);
            // console.log('login, token:', login, token);
            // res.cookie('login', login);
            // res.cookie('token', token, { expires: new Date(Date.now() + 900000), httpOnly: true });
            // console.log('req:', req.cookies);
            return res.send({ id: user._id, login: user.login, email: user.email, level: user.level, token });
    }

    @Post('register')
    async registerUser(
        @Body('login') login: string,
        @Body('password') password: string,
        @Body('email') email: string
    ) {
        const user = await this.usersSevice.createUser(login, password, email);
        return user;
    }

    @Put('update/:id')
    async updateUser(
        @Param('id') id: string,
        @Body() body: any,
    ) {
        await this.usersSevice.updateUser(id, body);
    }

    @Post('checkUser')
    async checkInfo(
        @Body('id') id: string,
        @Body('oldInfo') oldInfo: any,
        @Body('newInfo') newInfo: any
    ) {
        const result = await this.usersSevice.checkUser(id, oldInfo, newInfo);
        return result;
    }

    // @Post('check')
    // async checkAccount(
    //     @Req() req: any,
    //     @Body('login') login: string,
    //     @Body('token') token: string
    // ) {
    //     // console.log(login, token);
    //     // console.log('req.cookies.token:', req.cookies);
    //     if (token === req.cookies.token) {
    //         console.log('Пошло');
    //         const { level }: any = await this.usersSevice.getUserLevel(login);
    //         return level ? level : false;
    //     }
    //     return false;
    // }
}