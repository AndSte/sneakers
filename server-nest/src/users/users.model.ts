import { Schema, Document } from 'mongoose';
import { hash } from "bcrypt";
import { sign } from 'jsonwebtoken';

const UserSchema = new Schema({
    login: {type: String, required: true},
    password: {type: String, required: true, 
        validate: {
                validator: (value) => {
                    return value.length > 6;
            }
        }
    },
    email: {type: String, required: true, 
        validate: {
            validator: (v) => {
                return /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(v);
            }
        }
    },
    level: {type: Number, default: 0},
    bag: {type: Array, default: []}
}, {strict: true});

UserSchema.pre('save', async function (next) {
    const user: any = this;

    if(user.isModified('password')) {
        user.password = await hash(user.password, 8);
    }

    next();
});

// UserSchema.pre('updateOne', async function (next) {
//     const user: any = this;
//     console.log('user', user);
//     if(user.isModified('password')) {
//         user.password = await hash(user.password, 8);
//     }

//     next();
// });

UserSchema.methods.generateAuthToken = async function() {
    const user = this;
    // console.log('user', user);
    const token = sign({email: user.email.toString()}, 'akveduk');

    // console.log('token', token);
    return token;
}

export { UserSchema };

export interface IUser extends Document {
    login: string,
    password: string,
    email: string,
    level: number,
    bag: [],
    generateAuthToken()
}