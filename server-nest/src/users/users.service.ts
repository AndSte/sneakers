import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { compare, hash } from "bcrypt";

import { IUser } from "./users.model";
import { userInfo } from "os";

@Injectable()
export class UsersService {
    constructor(@InjectModel('User') private readonly userModel: Model<IUser>) {}

    async getAllUsers(): Promise<IUser[]> {
        const users = await this.userModel.find({});
        return users;
    }

    // async getUserLevel(login: string) {
    //     const user = await this.userModel.findOne({ login });
    //     return user ? user : false;
    // }

    async loginUser(
        email: string,
        password: string
    ): Promise<{user: IUser, token: string}> {

        const user = await this.userModel.findOne({email});
    
        if(!email) throw new Error('Incorrect login');
        
        const isMatch = await compare(password, user.password);
        
        if(!isMatch) throw new Error('Incorrect password');

        const token = await user.generateAuthToken();
        return { user, token };
    }

    async createUser(
        login: string,
        password: string,
        email: string,
    ): Promise<IUser> {
        const user = new this.userModel({
            login,
            password,
            email,
        });
        // user.password = await this.cryptPassword(user.password);
        await user.save();
        return user;
    }

    async checkUser(id: string, oldInfo: {
        password?: string,
        email?: string
    }, newInfo: {
        password?: string
        email?: string
    }) {
        const user = await this.userModel.findById(id);
        if(oldInfo.password && !newInfo.password || !oldInfo.password && newInfo.password) {
            return false;
        }

        if(oldInfo.password && newInfo.password) {
            const isMatch = await compare(oldInfo.password, user.password);
            if(!isMatch) {
                return false;
            }
            newInfo.password = await hash(newInfo.password, 8);
        }

        if(oldInfo.email && !newInfo.email || !oldInfo.email && newInfo.email || oldInfo.email && newInfo.email &&
            oldInfo.email !== user.email) {
            return false;
        }
        await this.updateUser(id, newInfo);
        return true;
    }

    async updateUser(id: string, body: {}) {
        await this.userModel.updateOne({_id: id }, body);
    }

    async getBag(id: string) {
        const user = await this.userModel.findById(id);
        return user.bag;
    }

    // private async cryptPassword(password) {
    //     return await hash(password, 10);
    // }
}