import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

import { MongooseModule } from "@nestjs/mongoose";
import { SneakersModule } from "./sneakers/sneakers.module";
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'client')
    }),
    MongooseModule.forRoot(
        'mongodb+srv://Andrey:1@sneakersproject-n3hpo.azure.mongodb.net/Sneakers?retryWrites=true&w=majority'
    ),
    SneakersModule,
    UsersModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
