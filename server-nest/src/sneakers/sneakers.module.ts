import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";

import { SneakersController } from "./sneakers.controller";
import { SneakersService } from "./sneakers.service";
import { SneakerSchema } from "./sneakers.model";

@Module({
    imports: [
        MongooseModule.forFeature([{name: "Sneaker", schema: SneakerSchema}])
    ],
    controllers: [SneakersController],
    providers: [SneakersService]
})
export class SneakersModule {}