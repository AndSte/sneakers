import {Controller, Get, Body, Param, Query, Delete, Post, UseInterceptors, UploadedFile, UploadedFiles, Put} from "@nestjs/common";
import { FilesInterceptor } from "@nestjs/platform-express";
import { SneakersService } from "./sneakers.service";


@Controller('api/sneakers')
export class SneakersController {

    constructor(private readonly sneakersService: SneakersService) {}

    @Get()
    async getAllSneakers() {
        const sneakers = await this.sneakersService.getAllSneakers();
        return sneakers;
    }

    @Get('categories')
    async getCategories() {
        const categories = await this.sneakersService.getCategories();
        return categories;
    }

    @Get('allCategories')
    async getAllCategories() {
        const categories = await this.sneakersService.getAllCategories();
        return categories;
    }

    @Get('search/:search')
    async searchSneakers(@Param('search') search: string) {
        const sneakers = await this.sneakersService.searchSneakers(search);
        return sneakers;
    }

    @Get('sort')
    async sortTest(
        @Query('titles') titles: string, 
        @Query('categories') categories: string,
        @Query('price') price: string,
        @Query('sizes') sizes: string
        ) {
        let arrayTitles, arrayCategories, arraySizes;
        if(titles) 
            arrayTitles = titles.split('_');
        if(categories)
            arrayCategories = categories.split('_');
        if(sizes)
            arraySizes = sizes.split('_');
        const sneakers = await this.sneakersService.sortByFilter(arrayTitles, arrayCategories, arraySizes, price);
        return sneakers;
    }

    @Post('add')
    @UseInterceptors(FilesInterceptor('files'))
    async addSneaker(
        @UploadedFiles() files: any[],
        @Body('name') name: string,
        @Body('model') model: string,
        @Body('category') category: string,
        @Body('sizes') sizes: any,
        @Body('description') description: string,
        @Body('price') price: number
    ) {
        console.log(files);
        const sneaker = await this.sneakersService.AddSneaker(
            name,
            model,
            category,
            description,
            sizes,
            price,
            files
        );
        return sneaker;
    }

    @Post('getArraySneakers')
    async getArraySneakers(@Body('arraySneakers') arraySneakers: []) {
        const result = this.sneakersService.getArraySneakers(arraySneakers);
        return result;
    }

    @Post(':id')
    @UseInterceptors(FilesInterceptor('files'))
    async editSneaker(
        @Param('id') id: string,
        @UploadedFiles() files: any[],
        @Body('name') name: string,
        @Body('model') model: string,
        @Body('category') category: string,
        @Body('sizes') sizes: any,
        @Body('description') description: string,
        @Body('price') price: number
    ) {
        await this.sneakersService.editSneaker(
            id, 
            name,
            model,
            category,
            description,
            sizes,
            price,
            files
        );
    }

    @Delete(':id')
    async deleteSneaker(@Param('id') id: string) {
        const sneaker = await this.sneakersService.deleteSneaker(id);
        return sneaker;
    }

    @Get('buffer/:id')
    async getBuffer(@Param('id') id: string) {
        const buffer = await this.sneakersService.getBuffer(id);
        return buffer;
    }

    @Get(':id')
    async getSneaker(@Param('id') id: string) {
        const sneaker = await this.sneakersService.getSneaker(id);
        return sneaker;
    }
}