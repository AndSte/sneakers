import {Injectable} from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from 'mongoose';
import * as fs from 'fs';
import * as path from 'path';
import { v1 as uuidv1 } from 'uuid';


import { ISneaker } from "./sneakers.model";
import { empty } from "rxjs";

@Injectable()
export class SneakersService {
    constructor(@InjectModel('Sneaker') private readonly sneakerModel: Model<ISneaker>) {}

    async getAllSneakers(): Promise<ISneaker[]> {
        const sneakers = await this.sneakerModel.find({});
        sneakers.forEach(item => {
            item.imgs.forEach(async elem => {
                const pathFile = path.join(__dirname, `../../client/${elem}`);
                const result = await this.fileRead(pathFile);
                // console.log('result:', result);
            });
        });
        return sneakers;
    }

    async getCategories() {
        const categories = await this.sneakerModel.find({}, {_id: 0, title: 1});
        // console.log('categories:', categories);
        const newCategories = categories.map(obj => {
            return obj.title;
        }).filter((title, index, arr) => arr.indexOf(title) === index);
        return newCategories;
    }

    async getAllCategories() {
        const sneakers = await this.sneakerModel.find({}, {_id: 0, title: 1, category: 1, sizes: 1});
        const categories = sneakers.reduce((item: any, current: any) => {
            // item.title = item.title.filter(item => item !== current.title);
            // item.category = item.category.filter(item => item !== current.category);
            if(!item.title.find(item => item === current.title))
                item.title.push(current.title);
            if(!item.category.find(item => item === current.category))
                item.category.push(current.category);
            
            current.sizes.forEach(size => {
                if(!item.sizes.find(item => item === size))
                    item.sizes.push(size);
            });
            
            return item;
        }, {
            title: [],
            category: [],
            sizes: []
        });

        return categories;
    }

    async getSneaker(id: string): Promise<ISneaker> {
        const sneaker = await this.sneakerModel.findById(id);
        return sneaker;
    }

    async getArraySneakers(arraySneaker) {
        const result = await Promise.all(arraySneaker.map(async item => await this.sneakerModel.findById(item)));
        return result;
    }

    async searchSneakers(search: string): Promise<ISneaker[]> {
        const regExp = new RegExp(search + '\\w?', 'ig');
        const sneakers = await this.sneakerModel.find({title: {$regex: regExp}});
        return sneakers;
    }

    async deleteSneaker(id: string): Promise<ISneaker> {
        const sneaker = await this.sneakerModel.findByIdAndDelete(id);
        return sneaker;
    }

    async AddSneaker(
        name: string,
        model: string,
        category: string,
        description: string,
        sizes: any,
        price: number,
        files: any[]
    ) {
        const imgsArray = [];
        files.forEach(async file => {
            const format = file.originalname.substring(file.originalname.lastIndexOf('.'));
            const imgName = `sneaker-${uuidv1()}${format}`;
            const imgPath = path.join(__dirname, '../../client/images/Sneakers', imgName);
            imgsArray.push(`images/Sneakers/${imgName}`);
            await this.writeFile(imgPath, file.buffer);
        });
        const arraySizes = sizes.split(',');
        const sneaker = new this.sneakerModel({
            title: name,
            model,
            category,
            description,
            imgs: imgsArray,
            sizes: arraySizes,
            price
        });
        await sneaker.save();
        return sneaker;
    }

    async editSneaker(
        id: string,
        name: string,
        model: string,
        category: string,
        description: string,
        sizes: any,
        price: number,
        files: any[]
    ) {
        const imgsArray = [];
        files.forEach(async file => {
            const format = file.originalname.substring(file.originalname.lastIndexOf('.'));
            const imgName = `sneaker-${uuidv1()}${format}`;
            const imgPath = path.join(__dirname, '../../client/images/Sneakers', imgName);
            imgsArray.push(`images/Sneakers/${imgName}`);
            await this.writeFile(imgPath, file.buffer);
        });
        const arraySizes = sizes.split(',');
        await this.sneakerModel.updateOne({_id: id}, {
            title: name,
            model,
            category,
            description,
            imgs: imgsArray,
            sizes: arraySizes,
            price
        });
    }

    async writeFile(filePath, data): Promise<any> {
        return new Promise((resolve, reject) => {
            fs.writeFile(filePath, data, err => {
                if(err)
                    reject(err);
                resolve('');
            });
        });
    }

    async fileRead(filepath) {
        return new Promise((resolve, reject) => {
            fs.readFile(filepath, (err, content) => {
                if(err)
                    throw Error;
                resolve(content);
            });
        });
    }

    async sortByFilter(titles: string[], categories: string[], sizes: string[], price: string) {
        const arrayAggregate = [];

        const objMatch: {
            title?: any,
            category?: any
            sizes?: any
        } = {};
        
        const objSort: {
            price?: number
        } = {};

        if(titles)
            objMatch.title = { $in: titles };

        if(categories)
            objMatch.category = { $in: categories };
            // objMatch.category = category;
        if(sizes)
            objMatch.sizes = { $all: sizes };
        
        if(price === "ascending")
            objSort.price = 1;
        if(price === "descending")
            objSort.price = -1;
        
        if(Object.keys(objMatch).length)
            arrayAggregate.push({ $match: objMatch });
        if(Object.keys(objSort).length)
            arrayAggregate.push({ $sort: objSort });

        const sneakers = await this.sneakerModel.aggregate(arrayAggregate);
        return sneakers;
    }

    async getBuffer(id: string) {
        const sneaker = await this.sneakerModel.findById(id);
        const buffer = [];
        await Promise.all(
            sneaker.imgs.map(async img => {
                const pathFile = path.join(__dirname, `../../client/${img}`);
                const file = await this.fileRead(pathFile);
                buffer.push(file);
            })
        );
        
        return buffer;
    }
    
    // async sneakersSortByPrice(param: boolean) {
    //     let sneakers;
    //     if(param)
    //         sneakers = await this.sneakerModel.aggregate( [{ $sort: { price: 1 }}] );
    //     else
    //         sneakers = await this.sneakerModel.aggregate( [{ $sort: { price: -1 }}] );
    //     return sneakers;
    // }

    // async sortBySizes(sizes: string[]) {
    //     // const sneakers = await this.sneakerModel.find({ sizes: { $all: sizes } });
    //     const sneakers = await this.sneakerModel.aggregate([ { $match: { sizes: { $all: sizes } } } ]);
    //     return sneakers;
    // }
}