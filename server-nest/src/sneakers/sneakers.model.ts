import { Schema, Document } from 'mongoose';

export const SneakerSchema = new Schema({
    title: {type: String, required: true},
    model: {type: String, required: true},
    category: {type: String, required: true},
    description: {type: String, required: true},
    imgs: {type: Array, required: true},
    sizes: {type: Array, default: []},
    price: {type: Number, required: true}
}, {strict: true});

export interface ISneaker extends Document {
    title: string,
    modelSneaker: string,
    category: string,
    description: string,
    imgs: string[],
    price: number
}