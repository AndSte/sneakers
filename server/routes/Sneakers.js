const {Router} = require('express');
const Sneakers = require('../models/Sneakers');

const router = Router();

router.get('/api/sneakers/', async (req, res) => {
    const sneakers = await Sneakers.find({});

    res.send(sneakers);
});

router.get('/api/sneakers/search/:search', async (req, res) => {
    const regExp = new RegExp(req.params.search + '\\w?', 'ig');
    console.log(regExp)
    const sneakers = await Sneakers.find({title: {$regex: regExp}});

    res.send(sneakers);
});

router.get('/api/sneakers/:id', async (req, res) => {
    const sneaker = await Sneakers.findById(req.params.id);

    res.send(sneaker);
});

router.post('/api/sneakers/add', async (req, res) => {
   const sneaker = new Sneakers({
       title: req.body.title,
       model: req.body.model,
       description: req.body.description,
       img: req.body.img,
       price: req.body.price
   });

   await sneaker.save();
   res.status(200).send();
});

module.exports = router;