const {Router} = require('express');
const Users = require('../models/Users');

const router = Router();

router.post('/users/login', async (req, res) => {
    try {
        const user = await Users.findOneByCredentials(req.body.login, req.body.password);

        res.status(200).send(user);
    } catch (e) {
        res.status(400).send(e.message);
    }
});

module.exports = router;