const mongoose = require('mongoose');

const UsersScheme = new mongoose.Schema({
    login: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    }
});


UsersScheme.statics.findOneByCredentials = async (login, password) => {
    const user = await User.findOne({login});

    if(!user)
        throw new Error('Incorrect login');
    if(user.password !== password)
        throw new Error('Incorrect password');

    return user;
}

const User = mongoose.model('User', UsersScheme);

module.exports = User;