const mongoose = require('mongoose');

const SneakerScheme = {
    title: {
        type: String,
        required: true
    },
    model: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    img: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    }
};

const Sneaker = mongoose.model('Sneaker', SneakerScheme);

module.exports = Sneaker;