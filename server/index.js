const express = require('express');
const mongoose = require('mongoose');
const sneakersRouter = require('./routes/Sneakers');
const usersRouter = require('./routes/Users');
const cors = require('cors');

const app = express();

app.use(cors());
app.use(express.json());
app.use(usersRouter);
app.use(sneakersRouter);
app.use(express.static(__dirname + '/public'));


const port = process.env.PORT || 5000;

async function start()
{
    try {
        await mongoose.connect('mongodb+srv://Andrey:1@sneakersproject-n3hpo.azure.mongodb.net/Sneakers', {
            useNewUrlParser: true,
            useCreateIndex: true
        });
        app.listen(port, () => console.log("Server started on port:", port));
        console.log('started');
    } catch(err) {
        console.log(err.message);
    }

}

start();