import Vue from 'vue'
import App from './App.vue'
import router from "./routes";
import store from "./store/index";
import VueCarousel from 'vue-carousel';

Vue.config.productionTip = false

Vue.use(VueCarousel);
// console.log(store);

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
