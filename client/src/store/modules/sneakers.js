import SneakerService from "../../api/sneakers/SneakerService";

const state = {
    sneakers: []
}

const getters = {
    getSneaker: state => id => state.sneakers.find(element => element._id === id),
}

const mutations = {
    setSneakers (state, sneakers) {
        state.sneakers = sneakers;
    },
    
    // deleteSneakers (state, id) {
    //     const indexElement = state.sneakers.findIndex(item => item._id === id);
    //     state.sneakers.splice(indexElement, 1);
    // }
}

const actions = {
    async getAllSneakers ({ commit }) {
        commit('setSneakers', await SneakerService.getSneakers());
    },
    async getSearchSneakers ({ commit }, search) {
        commit('setSneakers', await SneakerService.getSearchSneakers(search));
    },
    async addSneaker({ commit }, info) {
        await SneakerService.addSneaker(
            info.name,
            info.model,
            info.category,
            info.description,
            info.sizes,
            info.price,
            info.imgs
            );
        commit('setSneakers', await SneakerService.getSneakers());
    },
    async sortFilter({ commit }, fullPath) {
        commit('setSneakers', await await SneakerService.sortFilter(fullPath))
    },
    async editSneaker({ commit }, sneaker) {
        await SneakerService.editSneaker(sneaker.id, sneaker.info);
        commit('setSneakers', await SneakerService.getSneakers());
    },
    async deleteSneaker ({ commit }, search) {
        await SneakerService.deleteSneaker(search);
        commit('setSneakers', await SneakerService.getSneakers());
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}