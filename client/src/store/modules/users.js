import UserService from "../../api/users/UsersService";

const state = {
    id: localStorage.getItem('id'),
    login: localStorage.getItem('login'),
    email: localStorage.getItem('email'),
    level: localStorage.getItem('level'),
    token: ''
}

const getters = {
    getUser: state => {
        return {
            login: state.login,
            token: state.token
        }
    }
}

const mutations = {
    setUser(state, user) {
        state.id = user.id;
        state.login = user.login;
        state.email = user.email;
        state.level = user.level;
        state.token = user.token;

        console.log('state', state);
    },
    logOutUser(state) {
        state.id = '';
        state.login = '';
        state.email = '';
        state.level = '';
        state.token = '';
    },
    updateLogin(state, login) {
        localStorage.setItem('login', login);
        state.login = login;
    }
}

const actions = {
    async setUserAction({ commit }, user) {
        commit('setUser', await UserService.getAuthorization(user.email, user.password));
    },
    async logOutUserAction({ commit }) {
        commit('logOutUser');
        await UserService.logOutUser();
    },
    async getBag({ state }) {
        const bag = await UserService.getBag(state.id);
        return bag;
    },
    // async updateUser({ state }, newInfo, oldInfo = false) {
    //     let result = await UserService.updateUser(state.id, newInfo, oldInfo);
    //     console.log('result:', result);
    // }
    // async checkUser({ state }) {
    //     const result = await UserService.checkUser(state.login, state.token);
    //     console.log('result.level:', result);
    // }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}