const axios = require('axios');

const url = 'http://localhost:3000/api/sneakers';

class SneakerService{

    static async getSneakers()
    {
        let data;

        try {
            await axios.get(url).then(responce => {
                // console.log('responce.data', responce.data);
                data = responce.data;
            })
            console.log('data', data);
            return data;
            // return console.log('info:', info.data);
        } catch (e) {
            return e.message;
        }

    }

    static async getArraySneakers(arraySneakers) {
        try {
            const { data } = await axios.post(`${url}/getArraySneakers`, { arraySneakers });
            return data;
        } catch(e) {
            console.log('Error:', e.message);
        }
    }

    static async getSearchSneakers(search)
    {
        let data;
        try {
            await axios.get(url + '/search/' + search).then(responce => {
                data = responce.data;
            });
            return data;
        } catch (e) {
            return e.message;
        }
    }

    static async getSneaker(id)
    {
        let data;
        try {
            const newUrl = url + '/' + id;
            await axios.get(newUrl).then(responce => {
                data = responce.data;
            }).catch(e => console.log(e));
            return data;
        } catch (e) {
            return e.message;
        }
    }

    static async getCategories() {
        try {
            const data = await axios.get(url + '/categories');
            console.log('Вот', data.data);
            return data.data;
        } catch (e) {
            console.log(e.message);
            return e.message
        }
    }

    static async getAllCategories() {
        try {
            const { data } = await axios.get(url + '/allCategories');
            return data;
        } catch(e) {
            console.log(e.message);
            return e.message;
        }
    }

    static async addSneaker(
        name,
        model,
        category,
        description,
        sizes,
        price,
        files
    ) {
        try {
            const formData = new FormData();
            console.log('files', files);
            files.forEach(file => {
                formData.append('files', file);    
            });
            formData.append('name', name);
            formData.append('model', model);
            formData.append('category', category);
            formData.append('description', description);
            formData.append('sizes', sizes);
            formData.append('price', price)
            await axios.post(url + '/add', formData);
        } catch(e) {
            console.log(e.message);
            return e.message;
        }
    }

    static async editSneaker(id, info) {
        try {
            const formData = new FormData();
            console.log('Вот', info.imgs);
            info.imgs.forEach(file => {
                formData.append('files', file);    
            });
            formData.append('name', info.name);
            formData.append('model', info.model);
            formData.append('category', info.category);
            formData.append('description', info.description);
            formData.append('sizes', info.sizes);
            formData.append('price', info.price)
            await axios.post(`${url}/${id}`, formData);
        } catch(e) {
            console.log(e.message);
            return e.message;
        }
    }

    static async deleteSneaker(id) {
        try {
            await axios.delete(url + `/${id}`);
        } catch(e) {
            console.log(e.message);
            return e.message;
        }
    }

    static async sortFilter(fullPath) {
        try {
            const { data } = await axios.get(url + fullPath);
            return data;
        } catch(e) {
            console.log(e.message);
            return e.message;
        }
    }

    static async buffer(id = "5e49985ca83c4d2b44fbf45f") {
        try {
            const { data } = await axios.get(url + '/buffer/' + id);
            console.log('Data', data);
            return data;
        } catch(e) {
            console.log(e.message);
            return e.message;
        }
    }

}

// SneakerService.getSneakers();

export default SneakerService;