// import { createConnection } from 'mongoose';

const axios = require('axios');

const url = 'http://localhost:3000/api/users';

class UsersService {

    static async getAuthorization(email, password)
    {
        // let user = {};

        try {
            // await axios.post(url + '/login').then(responce => {
            //     console.log(responce.data);
            // });
            const { data } = await axios.post(url + '/login', {
                email,
                password
            });
            // console.log('user', user);
            localStorage.setItem('id', data.id);
            localStorage.setItem('login', data.login);
            localStorage.setItem('email', data.email);
            localStorage.setItem('level', data.level);
            
            return data;
        } catch(e) {
            return console.log(e.message);
        }
    }

    static async addUser(login, email, password) {
        try {
            await axios.post(url + '/register', {
                login,
                email,
                password
            });
            
        } catch(e) {
            return e.message;
        }
    }

    static async updateUser(id, newInfo, oldInfo = false) {
        try {  
            const { data } = await axios.post(`${url}/checkUser`, {
                id,
                oldInfo,
                newInfo
            });
            return data;
            // await axios.put(`${url}/update/${id}`, order);
        } catch(e) {
            console.log(e);
        }
    }

    static async logOutUser() {
        try {
            delete localStorage.id;
            delete localStorage.login;
            delete localStorage.email;
            delete localStorage.level;
            
            await axios.get(url + '/logout');
        } catch(e) {
            console.log(e.message);
        }
    }

    static async getBag(id) {
        try {
            const { data } = await axios.get(`${url}/${id}/bag`);
            return data;
        } catch(e) {
            console.log('Error:', e.message);
        }
    }

    // static async checkUser(login, token) {
    //     try {
    //         const { data } = await axios.post(url + '/check', {
    //             login,
    //             token
    //         });

    //         return data;
    //     } catch (e) {
    //         console.log(e.message);
    //     }
    // }
}

export default UsersService;