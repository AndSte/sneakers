import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from "../views/Home";
import Sneakers from "../views/Sneakers";
import Authorization from "../views/Authorization";
import AutorizationForm from '../components/profile/AuthorizationForm';
import RegistrationForm from '../components/profile/RegistrationForm';
import ActionSneakers from '../views/ActionSneakers';
import BagPage from '../views/BagPage';
import Profile from '../views/Profile'

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        component: Home
    },
    {
        path: '/sneakerPage/:id',
        component: Sneakers,

    },
    {
        path: '/search/:search',
        component: Home
    },
    {
        path: '/sort',
        component: Home
    },

    {
        path: '/add',
        name: 'AddSneaker',
        component: ActionSneakers,
        meta: {
            add: true
        }
    },
    {
        path: "/edit/:id",
        name: "editTask",
        component: ActionSneakers
    },
    {
        path: "/bag",
        name: "bag",
        component: BagPage
    },

    {
        path: '/authorization',
        component: Authorization,
        children: [
            {
                path: 'login',
                name: 'Login',
                component: AutorizationForm
            },
            {
                path: 'registration',
                name: 'Registration',
                component: RegistrationForm
            }
    ],
        redirect: {
            name: 'Authorization'
        }
    },

    {
        path: "/profile",
        name: 'Profile',
        component: Profile
    }
];

const router = new VueRouter({
    mode: 'history',
    routes
});

export default router;